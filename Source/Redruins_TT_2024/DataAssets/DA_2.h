// RedRuins Softworks (c)

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "DA_2.generated.h"

class UDA_1;

UENUM(Blueprintable, BlueprintType)
enum class EValueState : uint8
{
	value_true	UMETA(DisplayName = "Value True"),
	value_false	UMETA(DisplayName = "Value False"),
	value_n	UMETA(DisplayName = "Value N")
};
 
UCLASS()
class REDRUINS_TT_2024_API UDA_2 : public UPrimaryDataAsset
{
	GENERATED_BODY()

protected:
	UPROPERTY(BlueprintReadOnly, EditAnywhere, meta = (EditCondition = "ValueState == EValueState::value_true", EditConditionHides))
	TSoftClassPtr<UDA_1> DataAsset1;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	EValueState ValueState = EValueState::value_false;
};
