// RedRuins Softworks (c)

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "DA_1.generated.h"

class UDA_2;

USTRUCT(BlueprintType)
struct FData
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	TSoftClassPtr<UDA_2> DataAsset1;
	
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	FName Name;

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	FVector Vector;
};


UCLASS()
class REDRUINS_TT_2024_API UDA_1 : public UPrimaryDataAsset
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	FData Data;
};
/*
void UMyUserWidget::RemoveWidgetAndUpdateGrid(UWidget* WidgetToRemove)
{
	// Получаем индекс удаляемого виджета
	const int32 RemoveIndex = UniformGridPanel->GetChildIndex(WidgetToRemove);
	if (RemoveIndex != INDEX_NONE)
	{
		// Удаляем виджет
		UniformGridPanel->RemoveChildAt(RemoveIndex);

		// Обновляем положение всех виджетов после удаленного виджета
		for (int32 i = RemoveIndex; i < UniformGridPanel->GetChildrenCount(); i++)
		{
			UWidget* ChildWidget = UniformGridPanel->GetChildAt(i);
			UUniformGridSlot* ChildSlot = Cast<UUniformGridSlot>(ChildWidget->Slot);
			if (ChildSlot)
			{
				// Вычисляем новую позицию с учетом к-ва столбцов
				int32 NewRow = (i) / NumOfColumns;
				int32 NewColumn = (i) % NumOfColumns;

				// Установка новой позиции
				ChildSlot->SetRow(NewRow);
				ChildSlot->SetColumn(NewColumn);
			}
		}

		// Обновляем виджет, чтобы изменения применились сразу
		UniformGridPanel->ForceLayoutPrepass();
	}
}

void UMyUserWidget::RemoveWidgetAndUpdateGrid(UWidget* WidgetToRemove)
{
	// Получить индекс виджета, который будет удален
	const int32 RemoveIndex = UniformGridPanel->GetChildIndex(WidgetToRemove);
	if (RemoveIndex != INDEX_NONE)
	{
		// Удалить виджет из UniformGridPanel
		UniformGridPanel->RemoveChildAt(RemoveIndex);

		// Обновить позицию каждого виджета после удаленного
		int32 IndexOffset = 0;
		for (int32 i = RemoveIndex; i < UniformGridPanel->GetChildrenCount(); ++i)
		{
			UWidget* ChildWidget = UniformGridPanel->GetChildAt(i - IndexOffset);
			UUniformGridSlot* ChildSlot = Cast<UUniformGridSlot>(ChildWidget->Slot);

			if (ChildSlot)
			{
				// Рассчитать новую позицию внутри UniformGridPanel
				int32 NewRow = (i - IndexOffset) / NumOfColumns;
				int32 NewColumn = (i - IndexOffset) % NumOfColumns;

				// Удалить и заново добавить виджет на новую позицию, чтобы обновить компоновку
				UniformGridPanel->RemoveChildAt(i - IndexOffset);
				UniformGridPanel->AddChildToUniformGrid(ChildWidget, NewRow, NewColumn);
			}

			// После удаления дочернего виджета, индекс виджетов, которые идут за ним, уменьшается
			++IndexOffset;
		}
	}
}

void UMyUserWidget::RemoveWidgetAndUpdateGrid(UWidget* WidgetToRemove)
{
	UUniformGridPanel* GridPanel = MyUniformGridPanel; // Предполагаем, что MyUniformGridPanel уже инициализирован
	const int32 RemoveIndex = GridPanel->GetChildIndex(WidgetToRemove);

	if (RemoveIndex != INDEX_NONE)
	{
		GridPanel->RemoveChildAt(RemoveIndex);

		// Остановка, если удаляемый виджет был последним в сетке
		if (RemoveIndex == GridPanel->GetChildrenCount())
		{
			return;
		}

		for (int32 i = RemoveIndex; i < GridPanel->GetChildrenCount(); ++i)
		{
			UWidget* ChildWidget = GridPanel->GetChildAt(i);
			UUniformGridSlot* ChildSlot = Cast<UUniformGridSlot>(ChildWidget->Slot);

			if (ChildSlot)
			{
				// Определяем новые значения строки и столбца на основе индекса виджета вдоль GridPanel
				int32 NewRow = i / NumOfColumns;
				int32 NewColumn = i % NumOfColumns;

				// Обновляем позицию виджета в GridPanel
				ChildSlot->SetRow(NewRow);
				ChildSlot->SetColumn(NewColumn);
			}
		}

		// Принудительное обновление макета, чтобы изменения вступили в силу
		GridPanel->ForceLayoutPrepass();
	}
}*/