// RedRuins Softworks (c)

#pragma once

#include "UObject/NoExportTypes.h"
#include "TestObjectForTask04.generated.h"


UCLASS(Blueprintable)
class REDRUINS_TT_2024_API UTestObjectForTask04 : public UObject
{
	GENERATED_BODY()

public:
	virtual bool IsSupportedForNetworking() const override {return true;}
};
