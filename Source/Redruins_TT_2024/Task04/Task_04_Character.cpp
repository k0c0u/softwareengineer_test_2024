// RedRuins Softworks (c)


#include "Redruins_TT_2024/Task04/Task_04_Character.h"

#include "TestObjectForTask04.h"
#include "Engine/ActorChannel.h"
#include "Net/UnrealNetwork.h"

ATask_04_Character::ATask_04_Character()
{
	PrimaryActorTick.bCanEverTick = false;
}

void ATask_04_Character::BeginPlay()
{
	Super::BeginPlay();

	if (HasAuthority())
	{
		ServerCreateDynamicComponent();

		ServerAddUObject();
	}
}

void ATask_04_Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}


void ATask_04_Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

bool ATask_04_Character::ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags)
{
	bool bWroteSomthing = Super::ReplicateSubobjects(Channel, Bunch, RepFlags);
	if(IsValid(ObjectForTask04))
	{
		bWroteSomthing |= Channel->ReplicateSubobject(ObjectForTask04, *Bunch, *RepFlags);
	}
	return bWroteSomthing;
}

void ATask_04_Character::ServerAddUObject_Implementation()
{
	if(!IsValid(TestObjectForTask04))
	{
		UE_LOG(LogTemp, Warning, TEXT("ATask_04_Character::AddUObject- TestObjectForTask04Class not valid"));
		return;
	}

	ObjectForTask04 = NewObject<UTestObjectForTask04>(GetOwner(), TestObjectForTask04);
}

void ATask_04_Character::ServerCreateDynamicComponent_Implementation()
{
	DynamicMeshComponent = NewObject<UStaticMeshComponent>(this, "StaticMesh");
	if(IsValid(TESTMesh))
	{
		DynamicMeshComponent->SetStaticMesh(TESTMesh);
	}
	if(IsValid(DynamicMeshComponent))
	{
		DynamicMeshComponent->SetIsReplicated(true);
		DynamicMeshComponent->SetCollisionEnabled(ECollisionEnabled::Type::NoCollision);
		DynamicMeshComponent->AttachToComponent(GetRootComponent(), FAttachmentTransformRules::SnapToTargetNotIncludingScale);
		DynamicMeshComponent->RegisterComponent();
	}
}

void ATask_04_Character::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ATask_04_Character, ObjectForTask04);
	DOREPLIFETIME(ATask_04_Character, DynamicMeshComponent);
}
