// RedRuins Softworks (c)

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Task_04_Character.generated.h"

class UTestObjectForTask04;

UCLASS()
class REDRUINS_TT_2024_API ATask_04_Character : public ACharacter
{
	GENERATED_BODY()

public:
	ATask_04_Character();

protected:
	virtual void BeginPlay() override;

public:	
	
	virtual void Tick(float DeltaTime) override;
	
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	virtual bool ReplicateSubobjects(UActorChannel* Channel, FOutBunch* Bunch, FReplicationFlags* RepFlags) override;
	
	UFUNCTION(Server, Reliable)
	void ServerCreateDynamicComponent();

	UFUNCTION(Server, Reliable)
	void ServerAddUObject();
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Replicated, Category = "Test")
	TSubclassOf<UTestObjectForTask04> TestObjectForTask04;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Test")
	TObjectPtr<UStaticMesh> TESTMesh;

private:
	UPROPERTY(Replicated)
	TObjectPtr<UTestObjectForTask04> ObjectForTask04;

	UPROPERTY(Replicated)
	TObjectPtr<UStaticMeshComponent> DynamicMeshComponent;
};

